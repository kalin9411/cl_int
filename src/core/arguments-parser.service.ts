import { IArgumentsParser } from './../types/core/arguments-parser';
import { CommandParameters } from './../types/command-parameters/command-parameters';
import minimist from 'minimist';
import opts from 'minimist'

import { Injectable } from '../tools/decorators/injectable';

@Injectable()
export class ArgumentsParser implements IArgumentsParser {

  private readonly _arguments: CommandParameters;
  private readonly _command: string;

  constructor() {
    const args = minimist(process.argv.slice(2),{string: ['start', 'end','d']});

    // get the command name
    this._command = args._[0];
    // initialize the parsed _arguments object
    // console.log(`args are ${JSON.stringify(args)}`)

    this._arguments = {start:args.start, end:args.end, body:args.f, d:args.d,text: args.text};
  }

  public get arguments(): CommandParameters {
    return this._arguments;
  }

  public get command(): string {
    return this._command;
  }

}