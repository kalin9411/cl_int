interface IApi {
    resolve(text:string): Promise<ApiResult>
}

interface ApiResult {
    pods: IPods[];
    success: boolean;
    timing: string;
}

interface IPods {
    title: string;
    subpods: IsubPods[]
}
interface IsubPods {
    plaintext: string
}


export class ApiService implements IApi {

    constructor() {}

      public async resolve(text: string): Promise<ApiResult> {

          const WolframAlphaAPI = require('wolfram-alpha-api');
          const waApi = WolframAlphaAPI('5WLHTV-267K4857W8');
          const result: ApiResult = await waApi.getFull(text)
          return result
        }

}