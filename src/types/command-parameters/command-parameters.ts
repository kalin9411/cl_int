import { IntegralParameters } from './integral-parameters'
import { FreeTextParameters } from './freetext-parameters'

// merge command parameter types
export type CommandParameters = IntegralParameters | FreeTextParameters/* Replace with your joined command parameters */
