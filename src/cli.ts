import { CommandParameters } from './types/command-parameters/command-parameters';
import { ICommand } from './types/command';
import { ArgumentsParser } from './core/arguments-parser.service';
import { CommandContainer } from './commands/commands-container';
import { ConsolePrinter } from './core/console-printer.service';
import { Injectable } from './tools/decorators/injectable';
import { FormatterService } from './core/formatter.service';
import { ColorType } from './common/colors';
import { ExecutionResult } from './types/execution-result';

@Injectable()
export class CLI {

  constructor(
    private readonly parser: ArgumentsParser = new ArgumentsParser(),
    private readonly printer: ConsolePrinter = new ConsolePrinter(),
    private readonly commandContainer: CommandContainer,
    private readonly formatter: FormatterService = new FormatterService()
  ) { }
  public async main() {

    const command = this.parser.command as keyof CommandContainer;
    const args: CommandParameters = this.parser.arguments;

    // console.log('command is: '+command)
   // console.log(`in cli args are: ${JSON.stringify(args)}`)

    if (this.commandContainer[command] && typeof (this.commandContainer[command] as ICommand).execute === 'function') {
      const executionResult = await (this.commandContainer[command] as ICommand).execute(args);


      if (executionResult.errors) {
        this.printer.print(executionResult.message);
      }
  } else {this.printer.print(this.formatter.format(`${command} not recognized as command`,ColorType.Red))}
  }
}
