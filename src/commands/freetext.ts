
import { FormatterService } from '../core/formatter.service';
import { ConsolePrinter } from '../core/console-printer.service';
import { ExecutionResult } from '../types/execution-result';
import { ICommand } from '../types/command';
import { Injectable } from '../tools/decorators/injectable';
import { ColorType } from '../common/colors';
import { FreeTextParameters} from './../types/command-parameters/freetext-parameters'
import { ApiService } from '../core/api'


@Injectable()
export class FreeTextCommand implements ICommand {


  constructor(
    private readonly consolePrinter: ConsolePrinter,
    private readonly apiservice: ApiService,
    private readonly formatter: FormatterService,
  ) { }

  public async execute({text}: FreeTextParameters): Promise<ExecutionResult> {
    try {
      ///////////////////
      this.validateParams(text)
      const result = await this.apiservice.resolve(text)
      if (!result.success) { throw new RangeError('Computation Failed') }

      result.pods.forEach((element: any) => {
        console.log(`----------------
${this.formatter.format(element.title, ColorType.Orange)}
`); element.subpods.forEach((subpod: any) => {
          console.log(`${subpod.title}
${subpod.plaintext}
----`
          )
        })
      })

      return {errors:1, message: `${this.formatter.format(`Success`,ColorType.Green)} ${result.timing}s`}
    } catch (error) {
      return {
        errors: 1,
        message: this.formatter.format(error.message, ColorType.Red),
      };
    }
  }

  private validateParams(freetext:string) {
    if (freetext === undefined || freetext === null ) {throw new RangeError('Missing --text')}
  }

}