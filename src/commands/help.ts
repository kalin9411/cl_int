import { ConsolePrinter } from './../core/console-printer.service';
import { ICommand } from './../types/command';
import { ExecutionResult } from '../types/execution-result';
import { Injectable } from '../tools/decorators/injectable';

@Injectable()
export class HelpCommand implements ICommand {

  constructor(
    private readonly printer: ConsolePrinter = new ConsolePrinter(),
  ) { }

  public async execute(): Promise<ExecutionResult> {
    this.printer.print(
      `
      freetext
      --text

          example:

          >freetext --text="bernouli equation"


      integral                                optional
      --f : function to integrate             --start: from
      --d : differential                      --end: to

          double integral example:
          2 4
          ∫ ∫ (x+y) dx dy         >integral --f x+y --d x --d y --start 1 --end 2 --start 3 --end 4
          1 3
      `
    );

    return { errors: 0, message: ''};
  }

}