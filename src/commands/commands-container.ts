import { HelpCommand } from "./help";
import { Injectable } from "../tools/decorators/injectable";
import {IntegralCommand} from './integral'
import {FreeTextCommand} from './freetext'
@Injectable()
export class CommandContainer {

  constructor(
    public readonly help: HelpCommand,
    public readonly integral: IntegralCommand,
    public readonly freetext: FreeTextCommand
    ) { }
}