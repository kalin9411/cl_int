
import { FormatterService } from '../core/formatter.service';
import { ConsolePrinter } from '../core/console-printer.service';
import { ExecutionResult } from '../types/execution-result';
import { ICommand } from '../types/command';
import { Injectable } from '../tools/decorators/injectable';
import { ColorType } from '../common/colors';
import { IntegralParameters } from '../types/command-parameters/integral-parameters';
import { isNumber } from 'util';
import { ApiService } from '../core/api'


@Injectable()
export class IntegralCommand implements ICommand {



  constructor(
    private readonly consolePrinter: ConsolePrinter,

    private readonly formatter: FormatterService,
    private readonly apiservice: ApiService
  ) { }

  public async execute({start, end, body, d }: IntegralParameters): Promise<ExecutionResult> {
    //  try {
      this.validateParams(start, end, body, d)

      const joiner: string[] = []
      joiner.push(`integral of ${body}`)

      if (typeof d.forEach === 'function') {
      d.forEach((element,index)=>{
        joiner.push(`d${d[index]}`)
      })
      d.forEach((element,index)=> {
        if (start !== undefined) {
        joiner.push(` ${d[index]}=${start[index]}..${end[index]}`)}
      })
    }
    else {joiner.push(`d${d}`); if (start !== undefined) {
      joiner.push(` ${d}=${start}..${end}`)}
    }

      const interpretation = joiner.join(' ')
      this.consolePrinter.print(interpretation)

      const result = await this.apiservice.resolve(interpretation)
      if(!result.success) {throw new RangeError('Computation Failed')}
      this.consolePrinter.print(this.formatter.format(result.pods[0].title,ColorType.Orange))
      const plaintext: string =  result.pods[0].subpods[0].plaintext

      this.consolePrinter.print(plaintext)

      return {errors:1, message: `${this.formatter.format(`Success`,ColorType.Green)} ${result.timing}s`}
  //   } catch (error) {
  //     return {
  //       errors: 1,
  //       message: this.formatter.format(error.message, ColorType.Red),
  //     };
  //   }
  // }
  }
  private validateParams(start: string[] | undefined, end: string[] | undefined, body: string, diff: any) {
    if (body === undefined || body === null ) {throw new RangeError('Missing integral body')}
    if (diff === undefined || diff === null ) {throw new RangeError('Missing differential')}
    // if (!start && end || start && !end || (typeof start.forEach === 'function' && typeof end.forEach === 'function' && start.length !== end.length)) {throw new Error('Unmatched start - end')}



    if (typeof diff.forEach === 'function') {


    diff.forEach((element:any)=> {
    if (element.match(/\d+/)) {throw new Error('Differentials cannot contain numbers')}})} else {
    if (diff.match(/\d+/)) {throw new Error('Differential cannot contain numbers')}}

  }

}